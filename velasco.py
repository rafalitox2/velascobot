#!/usr/bin/env python3

import logging
import argparse
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
# from telegram.error import *
from archivist import Archivist
from speaker import Speaker

coloredlogsError = None
try:
    import coloredlogs
except ImportError as e:
    coloredlogsError = e

username = "velaskoespbot"
speakerbot = None

maxReadFile = 1E9
logger = logging.getLogger(__name__)

# Enable logging
log_format = "[{}][%(asctime)s]%(name)s::%(levelname)s: %(message)s".format(username.upper())

if coloredlogsError:
    logging.basicConfig(format=log_format, level=logging.INFO)
    logger.warning("Unable to load coloredlogs:")
    logger.warning(coloredlogsError)
else:
    coloredlogs.install(level=logging.INFO, fmt=log_format)

start_msg = "¡Hola amigos! Preguntame con /ayuda para sacarte una lista de los comandos."

wake_msg = "Me he despertao máquina"

info_msg = ("Bueno, el bot digamos que ahora si está en la versión 1.0, y en un nuevo servidor!\n"
           "Iré haciendo pruebas y si notáis algo ya sabéis.\n"
           "Para cualquier duda soy @rafalitox2 en telegram y para cualquier donación:\n"
           "paypal.me/rafalitox2")

help_msg = """Contesto a los siguientes comandos:

/empezar - Digo hola.
/yo - Quien soy.
/explicar - Te explico como funciono.
/ayuda - Envío este mensaje.
/cuenta - Te digo cuantos mensajes de este chat recuerdo.
/periodo - Cambia el periodo de mis mensajes. (Máximo de 100000)
/hablar - Me obligas a hablar.
/respuesta - Cambias la propabilidad de que responda al ser mencionado. (Decimal entre 0 y 1).
/restringir - Obligas a que solo los administradores puedan usar mis comandos (aunque seguiré hablando con todos).
/silencio - Evita las menciones a personas por el bot.
/quien - Digo información general sobre tí y tu mensaje. Para hacer debbug.
/configuracion - Digo la configuración de este chat.
/info - Te doy un poco de información mía y de mi autor.
/paypal - Pido €€
"""

about_msg = "Solo soy otro experimento de Markov. Leo todo lo que me escribes e intento contestarte con algo con sentido a lo que me digas.\n\nPuedes enviar /explicar si quieres más datos."

explanation = "Descompongo todos los mensajes que leo en grupos de 3 palabras, así guardo para cada par de palabras una tercera que le sigue. Uso esto para crear mis propios mensajes. Al principio solo repito los mensajes que escucho con esas 2 palabras o que yo crea que tiene significado.\n\nTambién separo mi vocabulario por chats, para solo decir en un chat lo que es de ese chat. Por privacidad, ya sabes. También guardo mi vocabulario en formato json, por lo que no hay logs.\n\nMi periodo por defecto en chats privados es de 1 respuesta por cada 2 mensajes recibidos, y si es un chat de grupo 10 mensajes leidos por cada mensaje que envío."

lasaña = "Los Lasaña 👍"

paypal = "Mi paypal es paypal.me/rafalitox2"

def static_reply(text, format=None):
    def reply(update, context):
        update.message.reply_text(text, parse_mode=format)
    return reply

def error(update, context):
    logger.warning('The following update:\n"{}"\n\nCaused the following error:\n'.format(update))
    logger.exception(context.error)
    # raise error


def stop(update, context):
    reader = speakerbot.get_reader(str(update.message.chat.id))
    # del chatlogs[chatlog.id]
    # os.remove(LOG_DIR + chatlog.id + LOG_EXT)
    logger.warning("I got blocked by user {} [{}]".format(reader.title(), reader.cid()))


def main():
    global speakerbot
    parser = argparse.ArgumentParser(description='A Telegram markov bot.')
    parser.add_argument('token', metavar='TOKEN',
                        help='The Bot Token to work with the Telegram Bot API')
    parser.add_argument('admin_id', metavar='ADMIN_ID', type=int, default=0,
                        help='The ID of the Telegram user that manages this bot')
    parser.add_argument('-w', '--wakeup', action='store_true',
                        help='Flag that makes the bot send a first message to all chats during wake up.')
    parser.add_argument('-f', '--filter', nargs='*', default=None, metavar='cid',
                        help='Zero or more chat IDs to add in a filter whitelist (default is empty, all chats allowed)')
    parser.add_argument('-n', '--nicknames', nargs='*', default=[], metavar='name',
                        help='Any possible nicknames that the bot could answer to.')
    parser.add_argument('-d', '--directory', metavar='CHATLOG_DIR', default='./chatlogs',
                        help='The chat logs directory path (default: "./chatlogs").')
    parser.add_argument('-c', '--capacity', metavar='C', type=int, default=20,
                        help='The memory capacity for the last C updated chats. (default: 20).')
    parser.add_argument('-m', '--mute_time', metavar='T', type=int, default=60,
                        help='The time (in s) for the muting period when Telegram limits the bot. (default: 60).')
    parser.add_argument('-s', '--save_time', metavar='T', type=int, default=3600,
                        help='The time (in s) for periodic saves. (default: 3600)')
    parser.add_argument('-p', '--min_period', metavar='MIN_P', type=int, default=1,
                        help='The minimum value for a chat\'s period. (default: 1)')
    parser.add_argument('-P', '--max_period', metavar='MAX_P', type=int, default=100000,
                        help='The maximum value for a chat\'s period. (default: 100000)')

    args = parser.parse_args()

    assert args.max_period >= args.min_period

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(args.token, use_context=True)

    filter_cids = args.filter
    if filter_cids:
        filter_cids.append(str(args.admin_id))

    archivist = Archivist(logger,
                          chatdir=args.directory,
                          chatext=".vls",
                          min_period=args.min_period,
                          max_period=args.max_period,
                          read_only=False
                          )

    username = updater.bot.get_me().username
    speakerbot = Speaker("@" + username,
                         archivist,
                         logger,
                         admin=args.admin_id,
                         cid_whitelist=filter_cids,
                         nicknames=args.nicknames,
                         wakeup=args.wakeup,
                         memory=args.capacity,
                         mute_time=args.mute_time,
                         save_time=args.save_time)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("empezar", static_reply(start_msg)))
    dp.add_handler(CommandHandler("yo", static_reply(about_msg)))
    dp.add_handler(CommandHandler("explicar", static_reply(explanation)))
    dp.add_handler(CommandHandler("ayuda", static_reply(help_msg)))
    dp.add_handler(CommandHandler("lasagna", static_reply(lasaña)))
    dp.add_handler(CommandHandler("paypal", static_reply(paypal)))
    dp.add_handler(CommandHandler("info", static_reply(info_msg)))
    dp.add_handler(CommandHandler("cuenta", speakerbot.get_count))
    dp.add_handler(CommandHandler("periodo", speakerbot.period))
    dp.add_handler(CommandHandler("lista", speakerbot.get_chats, filters=Filters.chat(chat_id=speakerbot.admin)))
    # dp.add_handler(CommandHandler("user", get_name, Filters.chat(chat_id=archivist.admin)))
    # dp.add_handler(CommandHandler("id", get_id))
    dp.add_handler(CommandHandler("parar", stop))
    dp.add_handler(CommandHandler("hablar", speakerbot.speak))
    dp.add_handler(CommandHandler("respuesta", speakerbot.answer))
    dp.add_handler(CommandHandler("restringir", speakerbot.restrict))
    dp.add_handler(CommandHandler("silencio", speakerbot.silence))
    dp.add_handler(CommandHandler("quien", speakerbot.who))
    dp.add_handler(CommandHandler("configuracion", speakerbot.where))
    dp.add_handler(CommandHandler("aodio", speakerbot.send_audio))

    # on noncommand i.e message - echo the message on Telegram
    # dp.add_handler(MessageHandler(Filters.text, echo))
    dp.add_handler(MessageHandler((Filters.text | Filters.sticker | Filters.animation), speakerbot.read))

    # log all errors
    dp.add_error_handler(error)

    speakerbot.wake(updater.bot, wake_msg)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
